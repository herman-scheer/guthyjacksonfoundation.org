import gulp from 'gulp'
import plugins from 'gulp-load-plugins'
import config from '../_/config/default'
const $ = plugins()

const destination = 'guthyjackson-560ed2b2ebad645b99000001'

gulp.task('icons-color', function() {
  return gulp.src(config.client.icons.color)
    .pipe($.svgmin())
    .pipe($.svgstore())
    .pipe($.rename(function(path) {
      path.basename = '_color'
      path.extname = '.html'

    }))
    .pipe(gulp.dest('./assets/svg'))
})

gulp.task('icons-no-color', function() {
  return gulp.src(config.client.icons.noColor)
    .pipe($.svgmin())
    .pipe($.svgstore())
    .pipe($.cheerio({
      run: function($) {
        $('[fill]').removeAttr('fill')
      },
      parserOptions: {
        xmlMode: true
      }
    }))
    .pipe($.rename(function(path) {
      path.basename = '_no-color'
      path.extname = '.html'
    }))
    .pipe(gulp.dest('./assets/svg'))
})

gulp.task('svg', ["icons-color", "icons-no-color"])