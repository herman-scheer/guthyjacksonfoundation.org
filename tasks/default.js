import gulp from 'gulp'
import runSequence from 'run-sequence'

gulp.task('build', function(done) {
  runSequence('images', 'svg', ['styles', 'scripts', 'views'], ['browser-sync', 'watch'], done)
})

gulp.task('default', function(done) {
  runSequence(['browser-sync', 'watch'], done)
})