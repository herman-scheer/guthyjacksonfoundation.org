import gulp from 'gulp'
import plugins from 'gulp-load-plugins'
import config from '../_/config/default'
import browserSync from 'browser-sync'
import path from 'path'

const $ = plugins()
// const destination = 'guthyjackson-560ed2b2ebad645b99000001'
const destination = 'assets/stylesheets'

gulp.task('styles', function() {
  return gulp.src(config.client.sass)
    .pipe(!config.minify ? $.sourcemaps.init() : $.util.noop())
    .pipe($.sass({
      indentedSyntax: true,
      includePaths: require('node-bourbon').with(config.client.lib.sass),
    }))
    .on('error', $.notify.onError())
    .pipe($.autoprefixer())
    .pipe($.rename(function(file) {
      // file.basename = '_main'
      file.basename = 'main'
      file.dirname = file.dirname.replace('sass', '')
      // file.extname = '.scss'
    }))
    .pipe(config.minify ? $.minifyCss() : $.util.noop())
    .pipe(!config.minify ? $.sourcemaps.write() : $.util.noop())
    .pipe(gulp.dest(destination))
    .pipe(browserSync.reload({
      stream: true
    }))
})