import gulp from 'gulp';
import config from '../_/config/default';

const destination = './guthyjackson-560ed2b2ebad645b99000001';

gulp.task('views-nb', function () {
  return gulp.src(config.client.viewsNB)
    .pipe(gulp.dest(destination));
});