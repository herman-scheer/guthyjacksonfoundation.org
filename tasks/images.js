import gulp from 'gulp'
import plugins from 'gulp-load-plugins'
import config from '../_/config/default'
import browserSync from 'browser-sync'

const reload = browserSync.reload
const $ = plugins()

gulp.task('images', function() {
  return gulp.src(config.client.images)
    .pipe($.newer('./assets/images'))
    .pipe($.imagemin())
    .pipe(gulp.dest('./assets/images'))
})

gulp.task('images-watch', ['images'], reload)