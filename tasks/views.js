import fs from 'fs'
import gulp from 'gulp'
import plugins from 'gulp-load-plugins'
import config from '../_/config/default'
import browserSync from 'browser-sync'
import path from 'path'
import yargs from 'yargs'

const $ = plugins()

gulp.task('test', function() {
  var stuff = require('../db.json')
  console.log(stuff)
})


// For rendering to php just change $.jade -> $.jadePhp

gulp.task('views', function() {
  return gulp.src(config.client.views)
    .pipe($.jadeGlobbing({
      placeholder: {
        'layout': yargs.argv.layout === 'trim' ? '_/views/layout-trim.jade' : '_/views/layout.jade'
      }
    }))
    .pipe($.jade({
      pretty: true,
      force: true,
      locals: {
        title: 'Guthy Jackson',
        data: require('../db.json'),
        frResearchers: require('../db/fr-researchers.json'),
        frInstitutions: require('../db/fr-institutions.json'),
        advocacyOrgs: require('../db/advocacy-orgs.json'),
        aoResources: require('../db/advocacy-orgs-resources.json'),
        reading: require('../db/reading.json'),
        videoFeatured: require('../db/video-featured.json'),
        videoQuestions: require('../db/video-questions.json'),
        videoFoundation: require('../db/video-foundation.json'),
        videoPublic: require('../db/video-public.json'),
        videoPatient2015: require('../db/video-nmo-patient-day-2015.json'),
        videoPatient2013: require('../db/video-nmo-patient-day-2013.json'),
        videoPatient2012: require('../db/video-nmo-patient-day-2012.json'),
        videoPatient2011: require('../db/video-nmo-patient-day-2011.json'),
        videoPatient2010: require('../db/video-nmo-patient-day-2010.json'),
        videoPatient2009: require('../db/video-nmo-patient-day-2009.json'),
        videoRoundtable2009: require('../db/video-nmo-roundtable-conf-2009.json'),
        diagnosis: require('../db/diagnosis.json'),
        sitemap: require('../db/sitemap.json'),
        facebookGroup: require('../db/facebook-groups.json')
      }
      
    }))
    // .pipe($.rename(function(file) {
    //   if (file.dirname === 'views/components')
    //     file.dirname = 'views'
    //   file.dirname = file.dirname.replace(path.sep + 'views', path.sep + 'html')
    // }))
    // // .pipe(gulp.dest('./assets'))
    // .pipe($.rename(function(file) {
    //   file.dirname = '.'
    // }))
    .pipe(gulp.dest('./_/html'))
})

gulp.task('views-watch', ['views'], browserSync.reload)