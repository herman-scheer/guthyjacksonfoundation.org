module.exports = {
  'jquery': '$',
  'modernizr': 'Modernizr',
  'foundation': {
    'exports': 'Foundation',
    'depends': {
      'jquery': 'jQuery'
    }
  },
  'slick': {
    'exports': 'slick',
    'depends': {
      'jquery': '$'
    }
  },
  'waypoints': {
    'exports': 'waypoints',
    'depends': {
      'jquery': '$'
    }
  },
  'angular': {
    'depends': {
      'jquery': '$'
    }
  },
  'accordion': {
    'depends': {
      'jquery': '$'
    }
  }
}