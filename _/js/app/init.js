module.exports = function(ApplicationConfiguration) {
  angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies)

  angular.module(ApplicationConfiguration.applicationModuleName)
    .config(function($locationProvider) {
      $locationProvider
        .html5Mode({
          enabled: true,
          requireBase: false
        })
    })
    .run(function($location, $rootElement) {
      window.addEventListener('mousewheel', function() {})
      $rootElement.off('click')
    })

  angular.element(document).ready(function() {
    if (window.location.hash === '#_=_') window.location.hash = '#!'
    angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName])
  })
}