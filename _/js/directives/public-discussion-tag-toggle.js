angular.module('core')
  //in Angular the name must be camel case
  .directive('publicDiscussionTagToggle', function() {
    return {
      restrict: 'C',
      link: function(scope, element, attrs) {
        
        var tagToggle = $('.gj-public-discussion__tag-toggle');
        var tagList = $('.tag-list');

        tagToggle.on('click', function () {
          tagList.toggleClass('active');
          console.log('hello')
        });
      }
    }
  });