angular.module('core')
  //in Angular the name must be camel case
  .directive('openCloseOffCanvas', function() {
    return {
      link: function(scope, element, attrs) {
        var off_canvas_trigger = $('.off-canvas-trigger')
        var off_canvas = $('.nav--link-items')

        off_canvas_trigger.on('click', function(event) {
          event.preventDefault();
          off_canvas.toggleClass('nav--link-items--open')
        });
      }
    }
  })