angular.module('core')
  .directive('stickySidebar', function() {

    return {
      restrict: 'AEC',
      link: function(scope, element, attrs) {

        $('.ui.sticky')
          .sticky({
            // context: '#page-with-sidebar',
            context: '.page-with-sidebar__wrapper',
            offset: 110
          })
      }
    }
  })