angular.module('core')
  .directive('svgInclude', [function() {
    return {
      scope: {
        className: '@',
        link: '@'
      },
      template: '<svg class="{{ className }}"><use xlink:href="{{ link }}"></use></svg>',
      replace: true
    }
  }])