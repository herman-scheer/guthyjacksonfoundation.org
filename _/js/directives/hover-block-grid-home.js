angular.module('core')
  .directive('hoverBlockGrid', function() {
    return {
      restrict: "AEC",
      link: function(scope, element, attrs) {

        var blockgrid_li = $('.nmo-get-involved__block-grid li')

        if (!Modernizr.touch) {
          blockgrid_li.hover(function() {
            $(this).toggleClass('hovered')
          })
        }
      }
    }
  })