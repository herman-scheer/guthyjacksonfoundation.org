angular.module('core')
  //in Angular the name must be camel case
  .directive('tagCategoryToggle', function() {
    return {
      restrict: 'C',
      link: function(scope, element, attrs) {
        
        var tagToggle = $('.gj-tag-list__toggle');
        var form = $('.gj-tag-list__ul');

        tagToggle.on('click', function () {
          form.toggleClass('active');
        });
      }
    }
  });