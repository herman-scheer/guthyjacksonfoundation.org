angular.module('core')
  .directive('modalGroupPodcast', function() {

    return {

      restrict: "AEC",
      link: function(scope, element, attrs) {

        var modalTrigger = $('.modal__trigger'),
            modal = $('.modal'),
            modalContent = $('.modal__content'),
            modalOverlay = $('<div/>').addClass('modal__overlay'),
            modalClose = $('<div/>').addClass('modal__close'),
            modalOverlayReal = $('.modal__overlay'),
            modalGroup = $('.modal-group-podcast');

        modalGroup.each(function() {
          modal.append(modalOverlay);
        });

        modalContent.append(modalClose);

        // Open modal
        modalTrigger.on('click', function() {
          var modalTarget = $(this).attr('href');
          $(modalTarget).addClass('open');
        })

        // Close modal
        /* overlay */
        $('.modal .modal__overlay').on('click', function() {
          $(this).parent('.modal').removeClass('open');
        })
        /* close icon */
         $('.modal .modal__content .modal__close').on('click', function() {
          $(this).closest('.modal').removeClass('open');
        });

      }

    };

  });