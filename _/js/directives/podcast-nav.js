angular.module('core')
  .directive('podcastMobileNav', function () {
    
    return {
      restrict: 'EC',
      link: function(scope, element, attrs) {

        const navLabel = $('.gj-podcast__nav-label')
        const navUl = $('.gj-podcast__nav-ul');

        navLabel.on('click', function() {
          navUl.toggleClass('active');
          console.log('clicked')
        });

      }
    };
  });