angular.module('core')
  .directive('accordion', function() {

    return {
      restrict: "AEC",
      link: function(scope, element, attrs) {

        var accordion = $('.accordion'),
          accordionContent = $('.accordion-content'),
          doc = $(document)

        doc.on('ready', function() {
          accordionContent.hide()
        })

        element.click(function(e) {
          e.preventDefault()
          $(this).siblings().removeClass('open')
          $(this).toggleClass('open')

          $(this).siblings().find(accordionContent).hide('400')
          $(this).find(accordionContent).slideToggle('400')
        })

        // reset sticky sidebar call
        // helps recalculate the height of the sidebar context
        function reinitStickySidebar() {
          $('.ui.sticky')
            .sticky({
              context: '#page-with-sidebar',
              offset: 110
            })
        }

        // call the sidebar initialization function 
        // with delay
        element.on('click', function() {
          setTimeout(reinitStickySidebar, 700);
        });

      }
    }

  })