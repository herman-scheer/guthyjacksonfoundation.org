angular.module('core')
  .directive('subSubNavDropDown', function() {
    return {
      link: function(scope, element, attrs) {

        if (!Modernizr.touch) {
          element.hover(function() {
            $(this).children('.sub-sub-wrap').toggleClass('show')
          })
        }
      }
    }
  })