angular.module('core')
  .directive('subNavDropDownClick', function() {
    return {
      link: function(scope, element, attrs) {

        var sub_trigger = $('.parent__inner-wrap a')
        var sub = $('.sub-wrap')

        sub_trigger.on('click', function(event) {
          event.preventDefault();
          sub.toggleClass('expand')
        });
      }
    }
  })