angular.module('core')
  .directive('modalToggle', function() {

    return {

      restrict: "C",
      link: function(scope, element, attrs) {

        var modalContainer = $('.gj-support-groups__modal-container');
        var modalClose = $('.gj-support-groups__modal-close');
        var formModal = $('.gj-support-groups__modal');
        var modalOverlay = $('.gj-support-groups__modal-overlay');

        element.on('click', function() {
          var formModal = $(this).attr('href');
          
          $(formModal).addClass('active');
        })

        modalClose.on('click', function() {
          formModal.removeClass('active');
        })

        modalOverlay.on('click', function() {
          formModal.removeClass('active');
        })

      }

    };
  });