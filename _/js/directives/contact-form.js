angular.module('core')
  .directive('formToggle', function() {
    return {
      restrict: "C",
      link: function(scope, element, attrs) {
        element.on('click', function() {
          element.toggleClass('active')
          element.next().toggleClass("active");
          console.log('form clicked')
        })
      }
    };
  });