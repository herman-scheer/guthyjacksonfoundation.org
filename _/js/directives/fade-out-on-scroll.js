angular.module('core')
  .directive('fadeOutOnScroll', function() {
    return {
      link: function(scope, element, attrs) {

        //Angular uses jQuery lite, so it has specific functions. For functions it doesn't have, like .css (see below) you have to wrap it in js:
        var fadeOutOnScroll = $(element);

        if (!Modernizr.touch) {
          $(window).on('scroll', function() {

            var offset = $(document).scrollTop(),
              fadeStart = 0,
              fadeUntil = 300,
              opacity = 0,
              transform = 'translate(0%, 100%)';
            if (offset <= fadeStart) {
              opacity = 1;
              transform = 'translate(0%, 0%)';
            } else if (offset <= fadeUntil) {
              opacity = 1 - offset / fadeUntil;
              transform = 'translate(0%,' + (2 * ($(this).scrollTop() / 10)) + '%)';
            }
            fadeOutOnScroll.css({
              'opacity': opacity,
              'transform': transform,
            });
          });
        }
      }
    }
  })