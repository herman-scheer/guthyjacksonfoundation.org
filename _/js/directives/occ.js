angular.module('core')
  .directive('occGroup', function() {

    return {
      restrict: 'AEC',
      link: function(scope, element, attrs) {

        var occ1 = $('.occ'),
          occ2 = $('.occ .occ__hidden .occ'),
          occ3 = $('.occ .occ__hidden .occ .occ__hidden .occ'),
          occReveal1 = $('.occ .occ__reveal'),
          occReveal2 = $('.occ .occ__hidden .occ .occ__reveal'),
          occReveal3 = $('.occ .occ__hidden .occ .occ__hidden .occ .occ_reveal'),
          occHidden = $('.occ__hidden'),
          occHidden1 = $('.occ .occ__hidden'),
          occHidden2 = $('.occ .occ__hidden .occ .occ__hidden'),
          occHidden3 = $('.occ .occ__hidden .occ .occ__hidden .occ .occ__hidden'),
          occBack1 = $('.occ .occ__hidden .occ__back'),
          occBack2 = $('.occ .occ__hidden .occ .occ__hidden .occ__back'),
          occBack3 = $('.occ .occ__hidden .occ .occ__hidden .occ .occ__hidden .occ__back'),
          occBackButton = $('<div/>').text('Back').addClass('occ__back'),
          oveflow = $('<div/>').text('').addClass('overlay'),
          doc = $(document)

        occHidden.wrapInner($('<div/>').text('').addClass('overflow'))
        $('.overflow').wrapInner($('<div/>').text('').addClass('overflow-inner'))

        occReveal1.on('click', function() {
          $(this).siblings(occHidden1).addClass('show')
          $(this).parent(occ1).addClass('active')
          occ1.addClass('fall-back')
          $(occ2, occ3).removeClass('fall-back')
        })

        occReveal2.on('click', function() {
          $(this).siblings(occHidden2).addClass('show')
          $(this).parent(occ2).addClass('active')
          occ2.addClass('fall-back')
          $(occ3).removeClass('fall-back')
          occBack1.addClass('fall-back')
          $(occBack2, occBack3).removeClass('fall-back')
        })

        occReveal3.on('click', function() {
          $(this).siblings(occHidden3).addClass('show')
          $(this).parent(occ3).addClass('active')
          occ3.addClass('fall-back')
        })

        occBack1.on('click', function() {
          $(this).closest(occHidden1).removeClass('show')
          $(this).closest(occ1).removeClass('active')
          occ1.removeClass('fall-back')
        })

        occBack2.on('click', function() {
          occHidden2.removeClass('show')
          $(this).closest(occ2).removeClass('active')
          occ1.addClass('fall-back')
          occ2.removeClass('fall-back')
          occBack1.removeClass('fall-back')
        })

        occBack3.on('click', function() {
          occHidden3.removeClass('show')
          $(this).closest(occ3).removeClass('active')
          occ3.removeClass('fall-back')
          $(occ1, occ2).addclass('fall-back')
        })

      }
    }

  })