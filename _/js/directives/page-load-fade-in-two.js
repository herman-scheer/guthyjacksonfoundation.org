angular.module('core')
  .directive('pageLoadFadeInTwo', function() {
    return {
      link: function(scope, element, attrs) {

        $(document).ready(function() {
          setTimeout(function() {
              element.addClass('fadein');
            },
            3600);
        });
      }
    }
  })