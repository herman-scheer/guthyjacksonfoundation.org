angular.module('core')
  //in Angular the name must be camel case
  .directive('publicDiscussionFormToggle', function() {
    return {
      restrict: 'C',
      link: function(scope, element, attrs) {
        
        var form = $('.form');
        var formToggle = $('.gj-public-discussion__create-topic');

        formToggle.on('click', function () {
          form.slideToggle(400);
        });
      }
    }
  });