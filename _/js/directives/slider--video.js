angular.module('core')
  .directive('videoSlider', function() {
    return {
      restrict: 'ECA',
      link: function(scope, element, attrs) {

        var sliderVideo = element.children('.vertical-slider__main')
        var sliderNav = element.children('.vertical-slider__nav').children('.vertical-slider__children')

      sliderVideo.slick({
          fade: true,
          arrows: false,
          slidesToShow: 1,
          slidesToScroll: 1,
          adaptiveHeight: true,
          draggable: true,
          asNavFor: sliderNav
        })

        sliderNav.slick({
          focusOnSelect: true,
          fade: false,
          arrows: false,
          slidesToShow: 5,
          slidesToScroll: 1,
          asNavFor: sliderVideo
        })

      }
    }
  })