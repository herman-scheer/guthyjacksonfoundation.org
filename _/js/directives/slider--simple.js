angular.module('core')
  .directive('simpleSlider', function() {
    return {
      restrict: 'ECA',
      link: function(scope, element, attrs) {

        var sliderSimple = element.children('.simple-slider__main')

        sliderSimple.slick({
          arrows: true,
          prevArrow: '<div class="previous-arrow slick-prev slick-arrow">' +
            '<svg><use xlink:href="#guthy-jackson-arrow-big-left"></use></svg>' +
            '</div>',
          nextArrow: '<div class="next-arrow slick-next slick-arrow">' +
            '<svg><use xlink:href="#guthy-jackson-arrow-big-right"></use></svg>' +
            '</div>',
          slidesToShow: 1,
          slidesToScroll: 1,
          draggable: true,
        })
      }
    }
  })