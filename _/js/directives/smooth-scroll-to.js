angular.module('core')
  .directive('smoothScroll', function() {
    return {
      restrict: "AEC",
      link: function(scope, element, attrs) {

        var has = window.location.hash;
        var link = $('a');
        var videoLink = $('.video-link');

        $(function() {

          // Don't smooth scroll for links on the videos pages
          if (!$('a').hasClass('video-link')) {
            $('a[href*=#]:not([href=#])').click(function() {
              if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash)
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']')
                if (target.length) {
                  $('html,body').animate({
                    scrollTop: target.offset().top
                  }, 1000)
                  return false
                }
              }
            })
          }

        })

        // Prevent scroll jump for links on videos pages
        videoLink.click(function(e) {
          e.preventDefault();
        })

      }
    }
  })