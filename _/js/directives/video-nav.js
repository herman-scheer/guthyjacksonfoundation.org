angular.module('core')
  .directive('videoNav', function () {
    
    return {
      restrict: 'EC',
      link: function(scope, element, attrs) {

        const navLabel = $('.gj-videos__nav-label'),
              navUl = $('.gj-videos__nav-ul'),
              catUl = $('.category-ul'),
              listUl = $('.list-ul'),
              catLabel = $('.gj-videos__category-label'),
              listLabel = $('.gj-videos__list-label'),
              listLink = $('.list-ul li a'),
              navLink = $('.gj-videos__nav-ul li a'),

              navLinkHref = navLink.attr('href'),
              video = $('.gj-videos__video'),

              firstVideo = $('.gj-videos__video:nth-child(1)'),
              firstLink = $('.list-ul li:nth-child(1) a');

        // add active class to first video
        firstVideo.addClass('active');
        firstLink.addClass('active');

        // active video link
        listLink.on('click', function() {
          listLink.removeClass('active');
          $(this).addClass('active');
        })

        // toggle nav with label
        navLabel.on('click', function() {
          if ($(this).hasClass('category-label')) {
            listUl.removeClass('active');
            $(this).next().toggleClass('active');
          } 
          if ($(this).hasClass('list-label')) {
            catUl.removeClass('active');
            $(this).next().toggleClass('active');
          }
        })

        // close nav when link is clicked
        navLink.on('click', function() {
          navUl.removeClass('active');
        })

        // switch video
        listLink.on('click', function() {
          const hrefId = $(this).attr('href');
          video.removeClass('active');
          $(hrefId).addClass('active');
        })

      }
    };
  });