angular.module('core')
  .directive('subSubNavDropDownClick', function() {
    return {
      link: function(scope, element, attrs) {

        var sub_trigger = $('.sub-sub a')
        var sub = $('.sub-sub-wrap')

        sub_trigger.on('click', function(event) {
          event.preventDefault();
          sub.toggleClass('expand')
        });
      }
    }
  })