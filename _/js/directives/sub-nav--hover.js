angular.module('core')
  .directive('subNavDropDown', function() {
    return {
      link: function(scope, element, attrs) {

        if (!Modernizr.touch) {
          element.hover(function() {
            $(this).children().children('.sub-wrap').toggleClass('active')
          })
        }
      }
    }
  })