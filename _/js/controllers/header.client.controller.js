//to add class on off-canvas
angular.module('core')
  .controller('HeaderController', function() {
    var vm = this

    //Off-canvas adding class to open it
    vm.isOpen = false
    vm.toggle = function() {
      vm.isOpen = !vm.isOpen
    }

    //Mobile Off-canvas sub-navs, toggling class to open and close them
    vm.toggleSub = function(id) {
      $(id).toggleClass('expand')
    }

  })