// Requires the config file
import ApplicationConfiguration from './app/config'

// Initiates Angular
require('./app/init')(ApplicationConfiguration)

// Registers our first module named core
ApplicationConfiguration.registerModule('core', [])

require('./controllers/header.client.controller')



// Require all your directives at the bottom of this.
require('./directives/svg')
require('./directives/open-close-off-canvas')
require('./directives/sub-nav--hover')
require('./directives/sub-nav--click')
require('./directives/sub-sub-nav--click')
require('./directives/sub-sub-nav--hover')
require('./directives/hover-block-grid-home')
require('./directives/page-load-fade-in-one')
require('./directives/page-load-fade-in-two')
require('./directives/fade-out-on-scroll')
require('./directives/smooth-scroll-to')
require('./directives/sticky-sidebar')
require('./directives/accordion')
require('./directives/slider--video')
require('./directives/slider--simple')
require('./directives/occ')
require('./directives/scroll-to-active')
require('./directives/modal')
require('./directives/modal-podcast')
require('./directives/video-nav')
require('./directives/contact-form')
require('./directives/form-modal')
require('./directives/podcast-nav')
require('./directives/public-discussion-form-toggle')
require('./directives/public-discussion-tag-toggle')
require('./directives/tag-category-toggle')
require('./directives/video-nav-home')