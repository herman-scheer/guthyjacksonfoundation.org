module.exports = {
  minify: process.env.NODE_ENV === 'production' ? true : false,
  client: {
    lib: {
      js: ['./_/js/vendor.js'],
      modernizr: ['./_/js/modernizr.js'],
      sass: [
        './node_modules/foundation-sites/scss',
        './node_modules/animate-sass-mixins'
      ],
      fonts: []
    },
    sass: [
      // './_/{scss,sass}/core.{scss,sass}',
      './_/{scss,sass}/**/*.{scss,sass}',
      './_/{scss,sass}/*.{scss,sass}'
    ],
    js: [
      './_/js/core.js',
      './_/js/app/init.js',
      './_/js/*.js',
      './_/js/**/*.js',
      '!_/js/vendor.js',
      '!_/js/modernizr.js'
    ],
    jsCore: ['./_/js/core.js'],
    images: ['./_/images/**/*.{png,jpg,gif,jpeg,ico}'],
    icons: {
      color: ['./_/svg/color/**/*.svg'],
      noColor: ['./_/svg/no-color/**/*.svg']
    },
    views: [
      // './_/views/*.jade',
      './_/views/pages/*.jade',
      './_/views/pages/*/**.jade',
      '!./_/views/**/layout.jade',
      '!./_/views/**/layout-trim.jade',
      '!./_/views/components/*.jade'
    ],
    viewsWatch: [
      './_/views/**/*.jade'
    ],
    viewsNB: './_/views-nb/*.html'
  },
  server: {
    allJS: ['gulpfile.js', 'config/**/*.js']
  }
}