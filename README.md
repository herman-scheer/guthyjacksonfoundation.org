# Guthy Jackson Install Guide

Here are the requirements to use the node environment to run all your task for this project.

  - Install node.js
  - Install gulp globally


////////////////////////////////////////////////////////////////////////////////////

## Install node
Download [node].


////////////////////////////////////////////////////////////////////////////////////

## Install gulp Globally
Once you have node installed, open your terminal and run this command.

```sh
$ npm install -g gulp
```

If that doesn't work you may need to add the `sudo` prefix and enter your admin password.

```sh
$ sudo npm install -g gulp
```


////////////////////////////////////////////////////////////////////////////////////

## Clone Repo

Then, from your terminal, `cd` into the folder you want to place the project and clone the repo.

```sh
$ git clone git@bitbucket.org:herman-scheer/guthyjacksonfoundation.org.git
```


////////////////////////////////////////////////////////////////////////////////////

## Install Project Dependencies

Once you've cloned the repo `cd` into the root of the Guthy Jackson project folder. Then install the project dependencies by running this:

```sh
$ npm install
```

This should take a while since there are a lot of dependecies. You can take a look at what the project is using by looking at the `package.json` file in the root.


////////////////////////////////////////////////////////////////////////////////////

## gulp Commands

Here's a list of gulp commands that will do different things depending on what you need. Type this into the terminal while you're at the root of the project folder.

```sh
$ gulp build
``` 
-- This command will build out the assets initially. Run this command the first time you run any gulp command in the project.

```sh
$ gulp
``` 
-- This will run [BrowserSync] and the `gulp watch` task. You don't need to worry too much about BrowserSync. Just let it run. The `watch` task is important since it will automate some tasks for you whenever you make a change in the sass, js, or jade fildes and upload the new assest the the Guthy Jackson AWS account where these assets live.

```sh
$ gulp watch
``` 
-- If you don't want to use BrowserSync, since it can take some time to load, you can just run `gulp watch` and simply watch the project folder for changes like I stated previously.

```sh
$ gulp images
``` 
-- This command will minify all the images in the project. It will take images in the `_/images` folder and place it into `assets/images`.

```sh
$ gulp views
``` 
-- This command will compile all your jade files into plain html. This will take all files in `_/views` and place them inside `_/html`.

```sh
$ gulp styles
``` 
-- This will compile all sass files. It will look inside `_/sass` and output a single .css file in `assets/stylesheets`.

```sh
$ gulp scripts
``` 
-- This will compile all script files. It will look inside `_/js` and output a single .js file into `assets/scripts`.

```sh
$ gulp svg
``` 
-- This will create a svg sprite inside the `_/svg` folder and place them in `assets/svg`. If you ever need to use this place any new svg inside the no-color folder. Once you output the new svg files in the assets folder you'll need to copy and paste the html code with the _no-color.html file that's in the NationBuilder `files` section under `Theme` when you're viewing all the pages in the backend.

```sh
$ gulp upload
``` 
-- This will upload all assets that are inside the `assets` folder. Run this command whenever you minify an image or if you simply want to make sure any newly outputted asset is uploaded to the AWS S3 account.

```sh
$ gulp build --env production
``` 
-- This is essentially the same thing as the previous `gulp build` command. When you include the `node env` flag at the `production` state it will run through all the build processes and also minify the scripts and css files. As you've probably guessed, you'll want to use this whenever you're ready to finalize any new css or script file. When you run this you'll also want to run the `gulp upload` command to make sure the assets get pushed to S3. 

```sh
$ gulp views --layout trim
``` 
-- As mentioned before, this will output the trimed html content that you'll want to copy and paste straight into the NationBuilder wysiwyg editor for basic pages. For more clarity feel free to run `gulp views` and then `gulp views --layout trim` and see the different outputs in the `_/html` folder.


////////////////////////////////////////////////////////////////////////////////////

## Local Development Tips

If you want to checkout out the site locally there are 2 methods to go about this. I'll start off with the more simple method.

### Method 1

Run `gulp` and visit `localhost:3000/{page-name}.html`. The name of the page must match a html file in the `_/html` folder. You won't see the nav/header or the footer of the site since those come from the NationBuilder temeplate files, but the css styling should be set as if those elements are there. From there you'll be able to adjust any sass file or script file. Since this isn't a true local build process you'll need to refresh the browser whenever you save a sass or script file and the upload process finishes (it should be pretty quick).

### Method 2

Method 2 is just an extention of method 1. Since your local build is referencing the same stylesheet and script files on S3, this method will show you how to sandbox your local script and stylesheet so it doesn't impact the live site.

Step 1: Open `tasks/styles.js` and look at line 22 that states, `file.basename = 'main'`. Change the base name of the compiles css file to anything you like. 

Step 2: Open `tasks/scripts.js` and look at line 46 that states, `.pipe(source('main.js'))`. Again change this to anything you like. 

Step 3: Run `gulp styles scripts` and check inside the assets folder to see if the newly changed names for those files are outputted properly. 

Step 4: Then run `gulp upload` to upload those files to S3.

Step 5: Open `_/views/layout.jade` and change the reference link for the stylesheet and main script file matches the new file names. 

Step 6: Run `gulp` again and check that your local dev is referencing the new files by inspecting the element or whatever you prefer.


////////////////////////////////////////////////////////////////////////////////////

## Conclusion

The code base is a bit messy, to my honest. This project was developed first by Tessa, then by me, throughout a period where we kept changing our dev workflow/best practices to increase code efficiency. Hence, much of the code is somewhat spaghetti and may not make too much sense, especially the css naming convensions. 

Also, the project was initially setup to use Angular, which is retrospec, was not the best idea since NationBuilder is not made to be too compatible with it. Nevertheless, it still works, but the down side is that if you ever want to add more scripts or update a script file, it will require you to use Angular. If you want to add more scripts then an easy way around this would be to create a stand-alone script file yourself, upload it to the S3 and call that file in the NationBuilder layout.html file in the theme. 

If you have any other questions feel free to contact me and I'll try to help you out as much as I can. 

   [node]: <https://nodejs.org/en/download/>
   [BrowserSync]: <https://www.browsersync.io/docs/gulp/>
